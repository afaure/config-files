#!/bin/bash

URL="http://www.tisseo.fr/prochains-passages/resultat?stop_num=19101&stop_id=1970324837186862&line_id=11821949021891674"
next_times=$(curl -s $URL | sed -n 's/.*<td class="td_horaires">\(.*\)<\/td>.*/\1/p')

first=$(echo $next_times | cut -d " " -f 1)
second=$(echo $next_times | cut -d " " -f 2)
echo $first
echo $second
