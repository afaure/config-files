set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
Plugin 'ntpeters/vim-better-whitespace'
highlight ExtraWhitespace ctermbg=red guibg=red

Plugin 'tpope/vim-fugitive'

" Syntastic plugin
"Plugin 'vim-syntastic/syntastic'
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" NERDTree
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
map <C-e> :NERDTreeToggle<CR>
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

" If there is no open file at startup open nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Tagbar
Plugin 'majutsushi/tagbar'
map <C-l> :TagbarToggle<CR>

" Golang
Plugin 'fatih/vim-go'

" Mark (highlight)
Plugin 'inkarkat/vim-ingo-library'
Plugin 'inkarkat/vim-mark'

" Vim airline (status bar)
Plugin 'vim-airline/vim-airline'

" ctrlp (fuzzy finder)
Plugin 'ctrlpvim/ctrlp.vim'

" Clang complete
" Plugin 'Rip-Rip/clang_complete'
" let g:clang_library_path = "/usr/lib/llvm-3.8/lib"
" let g:clang_use_library = 1

" Line at 80
"set colorcolumn=80
"set textwidth=80
" Color after 80
":au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

syntax on
"set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smart:tab
" Only do this part when compiled with support for autocommands.
if has("autocmd")
    " Use filetype detection and file-based automatic indenting.
    filetype plugin indent on

    " Use actual tab chars in Makefiles.
    autocmd FileType make set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab
    " 80 characters long lines
    " autocmd FileType python setlocal formatprg=par\ -w80\ -T4
endif

" For everything else, use a tab width of 4 space chars.
set tabstop=4       " The width of a TAB is set to 4.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of 4.
set shiftwidth=4    " Indents will have a width of 4.
set softtabstop=4   " Sets the number of columns for a TAB.
set expandtab       " Expand TABs to spaces.
set number          " Show line numbers
set visualbell      " Don't beep
set noerrorbells    " Don't beep
set wildmenu        " Visual autocomplete for command menu
set formatoptions+=w
" set tw=80

" Ignore uselss files
set wildignore=*.swp,*.bak,*.pyc,*.class,*.o
"set relativenumber  " Use relative numbers


" autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
" autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
" highlight EOLWS ctermbg=red guibg=red

