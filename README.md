# Vim :
- Install vundle :
`git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
- Launch vim, then enter :PluginInstall
