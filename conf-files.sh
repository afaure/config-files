#!/bin/bash

PATH_TO_REPO="/home/antoine/dev/linux-config-files"
PATH_ORIGIN=$(pwd)

# Conf files
CONF_I3="$HOME/.config/i3/config"
CONF_I3STATUS="$HOME/.i3status.conf"
CONF_GIT="$HOME/.gitconfig"
CONF_TMUX="$HOME/.tmux.conf"
CONF_VIM="$HOME/.vimrc"

# Save the config files, commit and push
function save {
    # Save i3 files
    cp $CONF_I3  $PATH_TO_REPO/i3/config
    cp $CONF_I3STATUS $PATH_TO_REPO/i3/i3status.conf

    # Save vim.rc
    cp $CONF_VIM $PATH_TO_REPO/vim/vimrc

    # Save git config file
    cp $CONF_GIT $PATH_TO_REPO/git/gitconfig

    # Save tmux config
    cp $CONF_TMUX $PATH_TO_REPO/tmux/tmux.conf

    # Commit and push
    cd $PATH_TO_REPO
    if [ $# -eq 3 -a -n $2 ]
    then
        git commit -am $2
    else
        git commit -am "Save config files"
    fi

    git push origin master
    cd $PATH_ORIGIN
}

# Pull the new config files and install them
function update {
    cd $PATH_TO_REPO
    git checkout master && git pull origin master

    # Update i3 files
    cp $PATH_TO_REPO/i3/config $CONF_I3
    cp $PATH_TO_REPO/i3/i3status.config $CONF_I3STATUS

    # Update vim.rc
    cp $PATH_TO_REPO/vim/vimrc $CONF_VIM

    # Update git config file
    cp $PATH_TO_REPO/git/gitconfig $CONF_GIT

    # Update tmux config
    cp $PATH_TO_REPO/tmux/tmux.conf $CONF_TMUX

    cd $PATH_ORIGIN
}

if [ $# -eq 1 ]
then
    echo "save / update ?"
elif [ $1 == "save" ]
then
    save $2
elif [ $1 == "update" ]
then
    update
else
    echo "save / update ?"
    return 1
fi
